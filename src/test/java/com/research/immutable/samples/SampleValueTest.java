package com.research.immutable.samples;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Immutable Sample Value Tests")
class SampleValueTest
{
    private SampleValue sampleValue;

    @BeforeEach
    public void setUp()
    {
        this.sampleValue = ImmutableSampleValue.builder().name("jason").age(49).myDogs(2).build();
    }

    @DisplayName("name test")
    @Test
    void name()
    {
        assertEquals("jason", this.sampleValue.name(), "I should be jason");
    }

    @DisplayName("age test")
    @Test
    void age()
    {
        assertEquals(49, this.sampleValue.age(), "I should be 49");
    }

    @DisplayName("myDogs test")
    @Test
    void myDogs()
    {
        assertEquals(2, this.sampleValue.myDogs(), "I have 2 dogs");
    }
}