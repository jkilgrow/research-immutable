package com.research.immutable.samples;

import org.immutables.value.Value;

@Value.Immutable
public abstract class SampleValue
{
    public abstract int myDogs();
    public abstract String name();
    public abstract int age();
}
