package com.research.immutable;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ResearchImmutableApplication
{

    public static void main(String[] args)
    {
        SpringApplication.run(ResearchImmutableApplication.class, args);
    }

}
